(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Crp
open Proto
module Table = Table
open Table.Conv
open Table

type txn = [ `Read | `Write ] Lmdb.Txn.t
type init_acc = unit

let init ?hook:_ ?n:_ () = rok @@ create ()

let transaction f =
  let err = ref None in
  let r = Lmdb.Txn.go Lmdb.Rw env (fun txn ->
      Lwt.bind (f txn) @@ function
      | Ok x -> rok x
      | Error e ->
        err := Some e;
        ignore @@ Lmdb.Txn.abort txn;
        rerr e) in
  match r with
  | Some x -> x
  | None -> match !err with
    | None -> rerr `txn_aborted
    | Some e -> rerr e

let register_predecessor ~txn b =
  let> r = get ~txn LPred b.hash in match r with
  | Ok (_, _, main) -> rok (`already_known main)
  | Error _ ->
    let>? () = add ~txn LPred b.hash
        (b.header.shell.predecessor, b.header.shell.level, false) in
    rok `new_block

let register ?block_hook ?operation_hook b =
  transaction @@ fun txn ->
  let>? r = register_predecessor ~txn b in
  match r with
  | `already_known main -> if main then rok `main else rok `alt
  | `new_block ->
    let block = Option.map (fun f -> (fun () b -> f txn b)) block_hook in
    let operation = Option.map (fun f -> (fun () o -> f txn o)) operation_hook in
    let|>? (), () = Utils.Process.block ?block ?operation ((), ()) b in
    `new_block

let registered hash =
  let> r = get LPred hash in match r with
  | Error _ -> rok (false, false)
  | Ok (_, _, main) -> rok (true, main)

let head txn =
  let> r = get ?txn LHead () in match r with
  | Ok (h, lv) -> rok (h, lv)
  | Error _ -> match txn with
    | None -> rok ("", 0l)
    | Some _ -> rerr `empty_database

let recursion txn hash =
  let> r = get ~txn LPred hash in match r with
  | Ok (pred, _lv, main) -> rok [ pred, main ]
  | Error _ -> rok []

let set_main ?main ?hook txn hash =
  let main = match main with Some _ -> true | None -> false in
  let>? (pred, level, _) = get ~txn LPred hash in
  let> _ = set LPred hash (pred, level, main) in
  match hook with
  | None -> rok (fun () -> rok ())
  | Some f -> f txn {m_hash = hash; m_main = main}

let forward_end ?hook level =
  match hook with
  | None -> rok ()
  | Some f -> f level
