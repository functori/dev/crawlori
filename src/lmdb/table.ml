(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

module Conv = Conv
open Conv

let env =
  Lmdb.Env.(create ~max_maps:1 ~flags:Flags.no_subdir ~map_size:274_877_906_944 Lmdb.Rw) (* 200 Go *)
    (".lmdb_" ^ Crp.database)

let close () = Lmdb.Env.close env

let key =
  Lmdb.Conv.make
    ~deserialise:(fun b -> fst @@ deser table_key_enc @@ {Binary.Reader.s = Crypto.Raw.mk @@ Bigstring.to_string b; offset=0})
    ~serialise:(fun _allow x -> Bigstring.of_string @@ (ser table_key_enc x :> string)) ()

let value =
  Lmdb.Conv.make
    ~deserialise:(fun b -> fst @@ deser table_value_enc @@ {Binary.Reader.s = Crypto.Raw.mk @@ Bigstring.to_string b; offset=0})
    ~serialise:(fun _allow x -> Bigstring.of_string @@ (ser table_value_enc x :> string)) ()


let create () = ignore @@ Lmdb.Map.(create Nodup ~key ~value) env

let connect ?txn () = Lmdb.Map.(open_existing Nodup ~key ~value ?txn) env
let drop ?txn () = Lmdb.Map.drop ~delete:true @@ connect ?txn ()

let get0 ?txn link = get link (Lmdb.Map.get ?txn (connect ?txn ()))
let remove0 ?txn link = remove link (Lmdb.Map.remove ?txn (connect ?txn ()))
let set0 ?txn link = set link (Lmdb.Map.set ?txn (connect ?txn ()))
let add0 ?txn link = set link (Lmdb.Map.add ?txn (connect ?txn ()))

let catch f =
  Lwt.catch f (function
      | Binary_conv e -> Crp.rerr (e :> Crp.error)
      | GADT_error -> Crp.rerr `gadt_error
      | exn -> Crp.rerr (`lmdb_exn exn))

let get ?txn link k = catch @@ fun () -> Crp.rok (get0 ?txn link k)
let remove ?txn link k = catch @@ fun () -> Crp.rok (remove0 ?txn link k)
let add ?txn link k v = catch @@ fun () -> Crp.rok (add0 ?txn link k v)
let set ?txn link k v = catch @@ fun () -> Crp.rok (set0 ?txn link k v)

let fold ?txn ?from ?until ?limit f acc =
  let map = connect ?txn () in
  let rec aux cursor acc limit =
    try
      let k, v = Lmdb.Cursor.next cursor in
      match until with
      | None -> aux2 cursor limit acc k v
      | Some ku when Lmdb.Map.compare_key map ku k > 0 ->
        aux2 cursor limit acc k v
      | _ -> acc
    with Not_found -> acc
  and aux2 cursor limit acc k v =
    match limit with
    | None -> begin match f acc k v with
        | None -> aux cursor acc None
        | Some acc -> aux cursor acc None end
    | Some n when n > 0 -> begin match f acc k v with
        | None -> aux cursor acc (Some n)
        | Some acc -> aux cursor acc (Some (n-1)) end
    | _ -> acc in
  Lmdb.Cursor.(go Lmdb.Rw map) (fun cursor ->
      match from with
      | None -> aux cursor acc limit
      | Some k ->
        let k, v = Lmdb.Cursor.seek_range cursor k in
        match until with
        | None -> aux2 cursor limit acc k v
        | Some ku when Lmdb.Map.compare_key map ku k > 0 ->
          aux2 cursor limit acc k v
        | _ -> acc)

let fold_rev ?from ?until ?txn ?limit f acc =
  let map = connect ?txn () in
  let rec aux cursor acc limit =
    try
      let k, v = Lmdb.Cursor.prev cursor in
      match until with
      | None -> aux2 cursor limit acc k v
      | Some ku when Lmdb.Map.compare_key map ku k < 0 ->
        aux2 cursor limit acc k v
      | _ -> acc
    with Not_found -> acc
  and aux2 cursor limit acc k v =
    match limit with
    | None -> begin match f acc k v with
        | None -> aux cursor acc None
        | Some acc -> aux cursor acc None end
    | Some n when n > 0 -> begin match f acc k v with
        | None -> aux cursor acc (Some n)
        | Some acc -> aux cursor acc (Some (n-1)) end
    | _ -> acc in
  Lmdb.Cursor.(go Lmdb.Rw map) (fun cursor ->
      match from with
      | None ->
        let k, v = Lmdb.Cursor.last cursor in
        aux2 cursor limit acc k v
      | Some k ->
        let k2, _ = Lmdb.Cursor.seek_range cursor k in
        if Lmdb.Map.compare_key map k k2 = 0 then
          ignore @@ Lmdb.Cursor.next cursor;
        match until with
        | None -> aux cursor acc limit
        | Some ku when Lmdb.Map.compare_key map ku k < 0 -> aux cursor acc limit
        | _ -> acc)
