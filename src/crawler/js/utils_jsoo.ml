open Ezjs_min
module SSet = Set.Make(String)

class type string_set = [js_string t] js_array

let set_of_jsoo (a : string_set t) =
  SSet.of_list (to_listf to_string a)

let set_to_jsoo (s : SSet.t) =
  of_listf string (SSet.elements s)
