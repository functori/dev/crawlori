(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Crawlori
open Crp_jsoo
open Make(Idb)(struct type extra = unit end)
open Ezjs_min_lwt

let get_config_aux js =
  let>? s =
    if to_string @@ typeof js = "string" then
      let> r = EzReq_lwt.get (EzAPI.URL (to_string @@ Unsafe.coerce js)) in
      match r with
      | Error (code, content) -> rerr @@ `request_error (code, content)
      | Ok json -> rok json
    else
      rok (to_string @@ _JSON##stringify js) in
  let json = EzEncoding.Ezjsonm.from_string s in
  try rok @@ Json_encoding.destruct (config_enc Json_encoding.unit) json
  with exn -> rerr (`cannot_parse_config exn)

let get_config js =
  let r = get_config_aux js in
  let r = Lwt.map (function
      | Ok x -> Ok x
      | Error e -> Error (error_of_string (Crp.string_of_error e))) r in
  Promise.promise_lwt_res r

let start config =
  EzLwtSys.run @@ fun () ->
  Lwt.map (Result.iter_error print_error) @@
  let>? () = init config in
  let|>? () = loop config in
  reset_stop ()

(* let bind ?(forward=false) f c txn x =
 *   let> r = Promise.to_lwt (Unsafe.fun_call f [|
 *       Unsafe.inject (config_to_jsoo ((fun () -> ()), (fun () -> ())) c);
 *       Unsafe.inject (bool forward);
 *       Unsafe.inject txn;
 *       Unsafe.inject x |]) in
 *   match r with
 *   | Error e -> rerr @@ `js_error (to_string e##toString)
 *   | Ok () -> rok ()
 *
 * let bind_main ?(forward=false) f c txn x =
 *   let> r = Promise.to_lwt (Unsafe.fun_call f [|
 *       Unsafe.inject (Crp_jsoo.config_to_jsoo ((fun () -> ()), (fun () -> ())) c);
 *       Unsafe.inject (bool forward);
 *       Unsafe.inject txn;
 *       Unsafe.inject x |]) in
 *   match r with
 *   | Error e -> rerr @@ `js_error (to_string e##toString)
 *   | Ok cb ->
 *     rok (fun () ->
 *         let> r = Promise.to_lwt (Unsafe.fun_call cb [||]) in
 *         match r with
 *         | Error e -> rerr @@ `js_error (to_string e##toString)
 *         | Ok () -> rok ())
 *
 * let bind_forward_end f c lv =
 *   let> r = Promise.to_lwt (Unsafe.fun_call f [|
 *       Unsafe.inject (config_to_jsoo ((fun () -> ()), (fun () -> ())) c);
 *       Unsafe.inject lv |]) in
 *   match r with
 *   | Error e -> Lwt.return_error @@ `js_error (to_string e##toString)
 *   | Ok () -> Lwt.return_ok ()
 *
 * let set_operation (f : (unit config_jsoo t -> bool t -> Idb.txn -> block_op_jsoo t -> unit Promise.promise t) callback) =
 *   Hooks.set_operation (fun ?forward c txn o -> bind ?forward f c txn (block_op_to_jsoo o))
 *
 * let set_block (f : (unit config_jsoo t -> bool t -> Idb.txn -> Proto_jsoo.full_block_jsoo t -> unit Promise.promise t) callback) =
 *   Hooks.set_block (fun ?forward c txn b -> bind ?forward f c txn @@ Proto_jsoo.full_block_to_jsoo b)
 *
 * let set_main (f : (unit config_jsoo t -> bool t -> Idb.txn -> main_arg_jsoo t -> unit Promise.promise t) callback) =
 *   Hooks.set_main (fun ?forward c txn m -> bind ?forward f c txn @@ main_arg_to_jsoo m)
 *
 * let set_main (f : (unit Cconfig_jsoo.jsoo t -> bool t -> Idb.txn -> Hooks_jsoo.main_arg_jsoo t -> (unit -> unit Promise.promise t) callback Promise.promise t) callback) =
 *   Hooks.set_main (fun c ?forward txn m -> bind_main ?forward f c txn @@ Hooks_jsoo.main_arg_to_jsoo m)
 *
 * let set_forward_end (f : (unit config_jsoo t -> int -> unit Promise.promise t) callback) =
 *   Hooks.set_forward_end (fun c lv -> bind_forward_end f c lv) *)

let () =
  Ezjs_min.export "crawlori" @@ object%js
    method load_config_ js = get_config js
    method start config = start config
    method stop = stop ()
    (* method set_block_hook_ f = set_block f
     * method set_operation_hook_ f = set_operation f
     * method set_main_hook_ f = set_main f
     * method set_forward_end_hook_ f = set_forward_end f *)
  end
