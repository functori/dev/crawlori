(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Crp
open Proto

let chain_id = "main"
let root = Printf.sprintf "chains/%s/blocks" chain_id

let rec request ?(index=0) ?(retries=0) ?last_error config path =
  if index >= List.length config.nodes then
    match config.retry with
    | Some r when r > retries ->
      let> () = EzLwtSys.sleep config.retry_timeout in
      request ~retries:(retries+1) config path
    | _ ->
      match last_error with None -> rerr `no_node_address | Some e -> rerr e
  else
    let node = List.nth config.nodes index in
    let full_path = Printf.sprintf "%s/%s/%s" node root path in
    let url = EzAPI.TYPES.URL full_path in
    Lwt.catch
      (fun () ->
         Lwt.bind (EzReq_lwt.get url) @@ function
         | Error (code, content) ->
           request ~index:(index+1) ~retries ~last_error:(`request_error (code, content)) config path
         | Ok s -> rok s)
      (fun exn ->
         request ~index:(index+1) ~retries ~last_error:(`request_exn exn) config path)

let request_enc config path encoding =
  let>? s = request config path in
  Lwt.catch
    (fun () -> rok (EzEncoding.destruct encoding s))
    (fun exn -> rerr (`destruct_exn exn))

let shell config hash = request_enc config (hash ^ "/header/shell") shell_enc.Encoding.json

let block config block_id = request_enc config block_id full_block_enc.Encoding.json

let head_hash config =
  let>? s = request config "head/hash" in
  rok (String.sub s 1 (String.length s - 3))

let head_metadata config = request_enc config "head/metadata" metadata_enc.Encoding.json
