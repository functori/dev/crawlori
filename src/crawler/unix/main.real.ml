(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Crawlori

module SMap = Map.Make(String)

module E = struct
  class type extra = object
    method allow_no_metadata : bool
    method accounts : SSet.t option
    val mutable contracts : Pg_bigmaps.bigmap_info SMap.t
    method set_contracts : Pg_bigmaps.bigmap_info SMap.t -> unit
    method contracts : Pg_bigmaps.bigmap_info SMap.t
  end [@@deriving encoding]

  let extra_enc : extra Json_encoding.encoding =
    let open Json_encoding in
    conv
      (fun e -> e#allow_no_metadata, Option.map SSet.elements e#accounts, SMap.bindings e#contracts)
      (fun (am, accs, cs)  ->
         let accs = Option.map SSet.of_list accs in
         let cs = List.fold_left (fun acc (k, v) -> SMap.add k v acc) SMap.empty cs in
         object
           method allow_no_metadata = am
           method accounts = accs
           val mutable contracts = cs
           method set_contracts s = contracts <- s
           method contracts = contracts
         end)
      (obj3
         (dft "allow_no_metadata" bool false)
         (opt "accounts" (list string))
         (dft "contracts" (assoc Pg_bigmaps.bigmap_info_enc) []))
  type init_acc = (string list * string list) list
end

let filename = ref None

let get_config () =
  Arg.parse [] (fun f -> filename := Some f) "Crawlori\nUsage:\n\tcrawlori conf.json";
  match !filename with
  | None -> rerr `no_config
  | Some f ->
    try rok (EzEncoding.(destruct (config_enc E.extra_enc) f))
    with _ ->
    try
      let ic = open_in f in
      let json = Ezjsonm.from_channel ic in
      close_in ic;
      rok Json_encoding.(destruct (config_enc E.extra_enc) json)
    with exn -> rerr (`cannot_parse_config exn)

module BM = Pg_bigmap.Make(struct
    let id = Z.zero
    let key = `tuple [ `nat; `address ]
    let value = `nat
    let contract = ""
    let always = false
    let keep = false
  end)

module BMS = Pg_bigmaps.Make(struct
    let fields = [
      "ledger", Some (`tuple [ `nat; `address ], `nat);
      "ledger", Some (`nat, `address);
    ]
    let always = false
    let keep = false
  end)

let () =
  EzLwtSys.run @@ fun () ->
  Lwt.map (Result.iter_error (fun e -> print_error e; exit 1)) @@
  let>? config = get_config () in
  match config.db_kind with
  | `pg ->
    let open Make(Pg)(E) in
    Plugins.register_mod (module struct type extra = E.extra type init_acc = Pg.init_acc include Pg_blocks end);
    Plugins.register_mod (module struct type extra = E.extra type init_acc = Pg.init_acc include Pg_operations end);
    Plugins.register_mod (module struct type extra = E.extra type init_acc = Pg.init_acc include BM end);
    Plugins.register_mod (module struct type extra = E.extra type init_acc = Pg.init_acc include BMS end);
    let>? () = init config in
    loop config
  | `lmdb ->
    let open Make(Lm)(E) in
    let>? () = init config in
    loop config
  | `caqti ->
    let open Make(Cq)(E) in
    let>? () = init config in
    loop config
  | _ ->
    Format.eprintf "the chosen db is not usable in unix@.";
    rok ()
