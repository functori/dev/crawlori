open Crp
open Proto
open Caqti_request.Infix

module Make(S : sig
    val id : Z.t
    val key : Mtyped.stype
    val value : Mtyped.stype
    val contract : string
    val always : bool
    val keep : bool
  end) = struct

  type nonrec txn = Cq.txn

  let always = S.always
  let name = "bigmap"
  let forward_end _config _level = rok ()
  let register_block ?forward:_ _ _ _ = rok ()

  let init _config acc =
    let upgrades = [
  "create table if not exists bigmap(\
   hash varchar not null, \
   key jsonb not null, \
   value varchar, \
   source varchar not null, \
   operation varchar not null, \
   index int not null, \
   block varchar not null, \
   level int not null, \
   tsp timestamp not null, \
   main boolean not null)" ] @
      if S.keep then [
        "alter table bigmap add constraint bigmap_id unique (hash, block, index)";
        "create index bigmap_hash_index on bigmap(hash)"
      ] else [
        "alter table bigmap add constraint bigmap_id primary key (hash, main)";
      ] in
    rok (acc @ upgrades)

  let get_bigmap_updates l =
    List.flatten @@
    List.filter_map (function
        | Big_map { id; diff = SDUpdate l }
        | Big_map { id; diff = SDAlloc {updates=l; _} } when id = S.id ->
          Some (List.filter_map (fun {bm_key; bm_value; bm_key_hash} ->
              match Mtyped.parse_value S.key bm_key with
              | Error _ -> None
              | Ok k ->
                match bm_value with
                | None -> Some (bm_key_hash, k, None)
                | Some value ->
                  match Mtyped.parse_value S.value value with
                  | Error _ -> None
                  | Ok v -> Some (bm_key_hash, k, Some v)) l)
        | _ -> None) l

  let row_type = Caqti_type.(
      tup3
        (tup4 string string (option string) string)
        (tup4 string int32 string int32)
        (tup2 ptime bool))

  let register_operation ?(forward=false) _config (module C : Caqti_lwt.CONNECTION) op =
    match op.bo_op.kind, op.bo_meta with
    | (Delegation _ | Reveal _ | Constant _), _ | _, None -> rok ()
    | Transaction {destination; _}, _ when destination <> S.contract -> rok ()
    | Origination _, _ when Tzfunc.Crypto.op_to_KT1 op.bo_hash <> S.contract -> rok ()
    | _, Some meta ->
      Log.(p ~color:`magenta @@ s "[%s] transaction %s" name (String.sub op.bo_hash 0 10));
      let l = get_bigmap_updates meta.op_lazy_storage_diff in
      iter (fun (hash, key, value) ->
          let key = EzEncoding.construct Mtyped.value_enc.json key in
          let value = Option.map (EzEncoding.construct Mtyped.value_enc.json) value in
          Log.(p ~color:`magenta @@ s "[%s] update %s : %s" name key (Option.value ~default:"none" value));
          let q = Caqti_type.(
              tup3
                row_type
                (tup3 (option string) string int32)
                (tup3 string int32 ptime) ->. unit)
              "insert into bigmap(hash, key, value, source, operation, \
               index, block, level, tsp, main) \
               values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?) on conflict (hash, main) do update \
               set value = ?, source = ?, index = ?, block = ?, level = ?, tsp = ?" in
          let tsp = Option.value ~default:Ptime.epoch @@ Ptime.of_float_s @@ CalendarLib.Calendar.to_unixfloat op.bo_tsp in
          Cq.wrap_lwt @@ C.exec q
            (((hash, key, value, op.bo_op.source),
              (op.bo_hash, op.bo_index, op.bo_block, op.bo_level),
              (tsp, forward)),
             (value, op.bo_op.source, op.bo_index),
             (op.bo_block, op.bo_level, tsp))) l

  let set_main ?(forward=false) _config (module C : Caqti_lwt.CONNECTION) m =
    let>? () =
      if not forward then
        if S.keep then
          let q = Caqti_type.(tup2 bool string ->. unit)
              "update bigmap set main = ? where block = ?" in
          Cq.wrap_lwt @@ C.exec q (m.m_main, m.m_hash)
        else
          let q = Caqti_type.(string ->* row_type)
              "delete from bigmap where block = ? returning *" in
          let>? l = Cq.wrap_lwt @@ C.collect_list q m.m_hash in
          iter (fun ((hash, key, value, source), (operation, index, block, level), (tsp, _main)) ->
              let q = Caqti_type.(tup2 bool string ->. unit)
                  "update bigmap set main = not ? where hash = ?" in
              let>? () = Cq.wrap_lwt @@ C.exec q (m.m_main, hash) in
              if m.m_main then
                let q = Caqti_type.(row_type ->. unit)
                    "insert into bigmap(hash, key, value, source, operation, \
                     index, block, level, tsp, main) \
                     values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" in
                Cq.wrap_lwt @@ C.exec q
                  ((hash, key, value, source), (operation, index, block, level), (tsp, true))
              else rok ()) l
      else rok () in
    rok (fun () -> rok ())

end
