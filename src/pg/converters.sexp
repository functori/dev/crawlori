(
 (
  (Rule (typnam entrypoint))
  (
   (serialize "EzEncoding.construct Proto.entrypoint_enc.Proto.Encoding.json")
   (deserialize "EzEncoding.destruct Proto.entrypoint_enc.Proto.Encoding.json")
  )
 )

 (
  (Rule (typnam micheline))
  (
   (serialize "EzEncoding.construct Proto.micheline_enc.Proto.Encoding.json")
   (deserialize "EzEncoding.destruct Proto.micheline_enc.Proto.Encoding.json")
  )
 )

 (
  (Or
   (
    (Rule (typnam zarith))
    (Rule (colnam gas_limit))
    (Rule (colnam storage_limit))
    (Rule (colnam counter))
    )
   )
  (
   (serialize Z.to_string)
   (deserialize Z.of_string)
  )
 )

 (
  (Rule (typnam op_status))
  (
   (serialize op_status_to_string)
   (deserialize op_status_of_string)
  )
 )

 (
  (Rule (typnam node_errors))
  (
   (serialize "EzEncoding.construct (Json_encoding.list Proto.node_error_enc.Proto.Encoding.json)")
   (deserialize "EzEncoding.destruct (Json_encoding.list Proto.node_error_enc.Proto.Encoding.json)")
  )
 )

 (
  (Rule (typnam balance_update_kind))
  (
   (serialize bu_kind_to_string)
   (deserialize bu_kind_of_string)
  )
 )
)
