open Crp
open Proto
open Pg_aux

type nonrec txn = txn

let always = false
let name = "blocks"
let register_operation ?forward:_ _config _dbh _op = rok ()
let forward_end _config _level = rok ()

let init _config acc =
  rok (acc @ [0, (Pg_blocks_tables.upgrade, Pg_blocks_tables.downgrade)])

let register_block ?(forward=false) config dbh (b : full_block) =
  match b.metadata, config#allow_no_metadata with
  | None, false -> rerr `block_without_metadata
  | _ ->
    let baker, cycle, cycle_position, expected_commitment = match b.metadata with
      | None -> None, None, None, None
      | Some m -> Some m.meta_baker, Some m.meta_level.nl_cycle,
                  Some m.meta_level.nl_cycle_position, Some m.meta_level.nl_expected_commitment in
    let h = b.header in
    let s = h.shell in
    let pow_nonce = (h.proof_of_work_nonce :> string) in
    [%pgsql dbh
        "insert into blocks(hash, level, cycle, cycle_position, expected_commitment, \
         predecessor, tsp, main, protocol, chain_id, baker, validation_pass, \
         operations_hash, fitness_version, round, pred_round, locked_round, context, \
         proof_of_work_nonce, seed_nonce_hash, signature) \
         values(${b.hash}, ${s.level}, $?cycle, $?cycle_position, \
         $?expected_commitment, ${s.predecessor}, ${s.timestamp}, $forward, ${b.protocol}, \
         ${b.chain_id}, $?baker, ${s.validation_pass}, \
         ${s.operations_hash}, ${s.fitness.fit_version}, ${s.fitness.fit_round}, \
         ${s.fitness.fit_pred_round}, $?{s.fitness.fit_locked}, ${s.context}, \
         $pow_nonce, $?{h.seed_nonce_hash}, ${h.signature}) on conflict do nothing"]

let set_main ?(forward=false) _config dbh m =
  let>? () =
    if not forward then
    [%pgsql dbh "update blocks set main = ${m.m_main} where hash = ${m.m_hash}"]
    else rok () in
  rok (fun () -> rok ())
