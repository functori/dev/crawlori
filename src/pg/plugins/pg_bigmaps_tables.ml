let downgrade = [
  "drop index bigmaps_block_index";
  "drop index bigmaps_contract_index";
  "drop index bigmaps_hash_index";
  "drop index contracts_block_index";
  "drop index contracts_address_index";
  "drop table bigmaps";
  "drop table contracts";
]

let upgrade ~keep = [
  "create table contracts(\
   address varchar primary key, \
   bigmap varchar not null, \
   name varchar not null, \
   key jsonb not null, \
   value jsonb not null, \
   source varchar not null, \
   operation varchar not null, \
   index int not null, \
   indexes int[] not null, \
   block varchar not null, \
   level int not null, \
   tsp timestamp not null, \
   main boolean not null)";
  "create table bigmaps(\
   contract varchar not null references contracts(address), \
   hash varchar not null, \
   key jsonb not null, \
   value jsonb, \
   source varchar not null, \
   operation varchar not null, \
   index int not null, \
   indexes int[] not null, \
   block varchar not null, \
   level int not null, \
   tsp timestamp not null, \
   main boolean not null, " ^
  (if keep then "unique (contract, hash, block, index))" else "primary key (contract, hash, main))");
  "create index contracts_address_index on contracts(address)";
  "create index contracts_block_index on contracts(block)";
  "create index bigmaps_block_index on bigmaps(block)";
  "create index bigmaps_contract_index on bigmaps(contract)";
  "create index bigmaps_hash_index on bigmaps(hash)";
]
