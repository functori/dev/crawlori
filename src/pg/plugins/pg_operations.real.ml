open Crp
open Proto
open Pg_aux
module PGOCaml = PG.PGOCaml

type nonrec txn = txn

let always = false
let name = "operations"

let forward_end _config _level = rok ()
let register_block ?forward:_ _config _dbh _b = rok ()

let init _config acc =
  rok (acc @ [0, (Pg_operations_tables.upgrade, Pg_operations_tables.downgrade)])

let op_status_to_string = function
  | `applied -> "applied" | `failed -> "failed"
  | `skipped -> "skipped" | `backtracked -> "backtracked"
let op_status_of_string = function
  | "applied" -> `applied | "failed" -> `failed | "skipped" -> `skipped
  | "backtracked" -> `backtracked | s -> failwith ("wrong op_status: " ^ s)

let get_options bo =
  let fee, gas_limit, storage_limit = match bo.bo_numbers with
    | None -> None, None, None
    | Some n -> Some n.fee, Some n.gas_limit, Some n.storage_limit in
  let status, errors = match bo.bo_meta with
    | None -> None, None
    | Some m -> Some m.op_status, Some m.op_errors in
  fee, gas_limit, storage_limit, status, errors

let op_indexes bo =
  let i0, i1, i2 = bo.bo_indexes in
  [ Some i0; Some i1; Some i2 ]

let register_transaction ?(forward=false) ~dbh ~amount ~destination ?parameters bo =
  let fee, gas_limit, storage_limit, status, errors = get_options bo in
  let indexes = op_indexes bo in
  let entrypoint, parameters = match parameters with
    | None -> None, None
    | Some p -> Some p.entrypoint, Some p.value in
  [%pgsql dbh
      "insert into transactions(hash, amount, destination, entrypoint, parameters, \
       source, fee, gas_limit, storage_limit, counter, nonce, index, indexes, \
       status, errors, block, level, tsp, main) \
       values(${bo.bo_hash}, $amount, $destination, $?entrypoint, $?parameters, \
       ${bo.bo_op.source}, $?fee, $?gas_limit, $?storage_limit, ${bo.bo_counter}, \
       $?{bo.bo_nonce}, ${bo.bo_index}, $indexes, $?status, $?errors, \
       ${bo.bo_block}, ${bo.bo_level}, ${bo.bo_tsp}, $forward) on conflict do nothing"]

let register_origination ?(forward=false) ~dbh ~balance ~script bo =
  let fee, gas_limit, storage_limit, status, errors = get_options bo in
  let indexes = op_indexes bo in
  [%pgsql dbh
      "insert into originations(hash, balance, code, storage, \
       source, fee, gas_limit, storage_limit, counter, nonce, index, indexes, \
       status, errors, block, level, tsp, main) \
       values(${bo.bo_hash}, $balance, ${script.code}, ${script.storage}, \
       ${bo.bo_op.source}, $?fee, $?gas_limit, $?storage_limit, ${bo.bo_counter}, \
       $?{bo.bo_nonce}, ${bo.bo_index}, $indexes, $?status, $?errors, \
       ${bo.bo_block}, ${bo.bo_level}, ${bo.bo_tsp}, $forward) on conflict do nothing"]

let register_delegation ?(forward=false) ~dbh ?delegate bo =
  let fee, gas_limit, storage_limit, status, errors = get_options bo in
  let indexes = op_indexes bo in
  [%pgsql dbh
      "insert into delegations(hash, delegate, \
       source, fee, gas_limit, storage_limit, counter, nonce, index, indexes, \
       status, errors, block, level, tsp, main) \
       values(${bo.bo_hash}, $?delegate, ${bo.bo_op.source}, \
       $?fee, $?gas_limit, $?storage_limit, ${bo.bo_counter}, $?{bo.bo_nonce}, \
       ${bo.bo_index}, $indexes, $?status, $?errors, \
       ${bo.bo_block}, ${bo.bo_level}, ${bo.bo_tsp}, $forward) on conflict do nothing"]

let register_reveal ?(forward=false) ~dbh ~pk bo =
  let fee, gas_limit, storage_limit, status, errors = get_options bo in
  let indexes = op_indexes bo in
  [%pgsql dbh
      "insert into reveals(hash, pubkey, \
       source, fee, gas_limit, storage_limit, counter, nonce, index, indexes, \
       status, errors, block, level, tsp, main) \
       values(${bo.bo_hash}, $pk, ${bo.bo_op.source}, \
       $?fee, $?gas_limit, $?storage_limit, ${bo.bo_counter}, $?{bo.bo_nonce}, \
       ${bo.bo_index}, $indexes, $?status, $?errors, \
       ${bo.bo_block}, ${bo.bo_level}, ${bo.bo_tsp}, $forward)"]

let register_operation ?forward (config : < accounts : SSet.t option; .. >) dbh bo =
  let relevant = Utils.AccountRelevant.info_opt config#accounts bo.bo_op in
  if not relevant then rok ()
  else
    match bo.bo_op.kind with
    | Transaction {amount; destination; parameters} ->
      register_transaction ?forward ~dbh ~amount ~destination ?parameters bo
    | Origination {balance; script} ->
      register_origination ?forward ~dbh ~balance ~script bo
    | Delegation delegate ->
      register_delegation ?forward ~dbh ?delegate bo
    | Reveal pk ->
      register_reveal ?forward ~dbh ~pk bo
    | _ -> rok ()

let set_main ?(forward=false) _config dbh m =
  let>? () =
    if not forward then
      let>? () = [%pgsql dbh "update transactions set main = ${m.m_main} where hash = ${m.m_hash}"] in
      let>? () = [%pgsql dbh "update originations set main = ${m.m_main} where hash = ${m.m_hash}"] in
      let>? () = [%pgsql dbh "update delegations set main = ${m.m_main} where hash = ${m.m_hash}"] in
      let>? () = [%pgsql dbh "update reveals set main = ${m.m_main} where hash = ${m.m_hash}"] in
      rok ()
    else rok () in
  rok (fun () -> rok ())
