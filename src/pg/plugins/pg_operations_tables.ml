let downgrade = [
  "drop index transactions_hash_index";
  "drop index transactions_block_index";
  "drop index transactions_level_index";
  "drop index transactions_source_index";
  "drop index transactions_destination_index";
  "drop index originations_hash_index";
  "drop index originations_block_index";
  "drop index originations_level_index";
  "drop index originations_source_index";
  "drop index delegations_hash_index";
  "drop index delegations_block_index";
  "drop index delegations_level_index";
  "drop index delegations_source_index";
  "drop index delegations_delegate_index";
  "drop index reveals_hash_index";
  "drop index reveals_block_index";
  "drop index reveals_level_index";
  "drop index reveals_source_index";
  "drop index balance_updates_account_index";
  "drop index balance_updates_block_index";
  "drop index balance_updates_level_index";
  "drop index balance_updates_operation_index";
  "drop table transactions";
  "drop table originations";
  "drop table delegations";
  "drop table reveals";
  "drop table balance_updates";
  "drop table blocks";
  "drop table predecessors";
  "drop domain entrypoint";
  "drop domain micheline";
  "drop type op_status";
  "drop domain node_errors";
  "drop domain zarith";
  "drop type balance_update_kind"
]

let upgrade = [
  "create domain entrypoint as varchar";
  "create domain micheline as jsonb";
  "create domain zarith as numeric";
  "create type op_status as enum ('applied', 'failed', 'skipped', 'backtracked')";
  "create domain node_errors as jsonb";
  "create or replace function zadd(z1 zarith, z2 zarith) returns zarith as $$ \
   begin return z1 + z2; end; $$ language plpgsql";
  "create or replace function zsub(z1 zarith, z2 zarith) returns zarith as $$ \
   begin return z1 - z2; end; $$ language plpgsql";
  "create or replace function zmul(z1 zarith, z2 zarith) returns zarith as $$ \
   begin return z1 * z2; end; $$ language plpgsql";
  "create or replace function zdiv(z1 zarith, z2 zarith) returns zarith as $$ \
   begin return z1 / z2; end; $$ language plpgsql";

  "create table transactions(\
   hash varchar not null, \
   amount bigint not null, \
   destination varchar not null, \
   entrypoint entrypoint, \
   parameters micheline, \
   source varchar not null, \
   fee bigint, \
   gas_limit zarith, \
   storage_limit zarith, \
   counter zarith not null, \
   nonce int, \
   index int not null, \
   indexes int[] not null, \
   status op_status, \
   errors node_errors, \
   block varchar not null, \
   level int not null, \
   tsp timestamp not null, \
   main boolean not null)";

  "create table originations( \
   hash varchar not null, \
   balance bigint not null, \
   code micheline not null, \
   storage micheline not null, \
   source varchar not null, \
   fee bigint, \
   gas_limit zarith, \
   storage_limit zarith, \
   counter zarith not null, \
   nonce int, \
   index int not null, \
   indexes int[] not null, \
   status op_status, \
   errors node_errors, \
   block varchar not null, \
   level int not null, \
   tsp timestamp not null, \
   main boolean not null)";

  "create table delegations( \
   hash varchar not null, \
   delegate varchar, \
   source varchar not null, \
   fee bigint, \
   gas_limit zarith, \
   storage_limit zarith, \
   counter zarith not null, \
   nonce int, \
   index int not null, \
   indexes int[] not null, \
   status op_status, \
   errors node_errors, \
   block varchar not null, \
   level int not null, \
   tsp timestamp not null, \
   main boolean not null)";

  "create table reveals( \
   hash varchar not null, \
   pubkey varchar not null, \
   source varchar not null, \
   fee bigint, \
   gas_limit zarith, \
   storage_limit zarith, \
   counter zarith not null, \
   nonce int, \
   index int not null, \
   indexes int[] not null, \
   status op_status, \
   errors node_errors, \
   block varchar not null, \
   level int not null, \
   tsp timestamp not null, \
   main boolean not null)";

  "create index transactions_hash_index on transactions(hash)";
  "create index transactions_block_index on transactions(block)";
  "create index transactions_level_index on transactions(level)";
  "create index transactions_source_index on transactions(source)";
  "create index transactions_destination_index on transactions(destination)";
  "create index originations_hash_index on originations(hash)";
  "create index originations_block_index on originations(block)";
  "create index originations_level_index on originations(level)";
  "create index originations_source_index on originations(source)";
  "create index delegations_hash_index on delegations(hash)";
  "create index delegations_block_index on delegations(block)";
  "create index delegations_level_index on delegations(level)";
  "create index delegations_source_index on delegations(source)";
  "create index delegations_delegate_index on delegations(delegate)";
  "create index reveals_hash_index on reveals(hash)";
  "create index reveals_block_index on reveals(block)";
  "create index reveals_level_index on reveals(level)";
  "create index reveals_source_index on reveals(source)";
]
