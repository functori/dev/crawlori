(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

let s = Format.sprintf

let p ?color ?(before="") ?(after="") ?(en=true) s =
  let color, fin = match color with None -> "", "" | Some d -> Format.sprintf "\027[0;%dm" (Color.to_unix_int d), "\027[0m" in
  if en then
    Format.printf "%s%s%s%s%s@." before color s fin after
  else
    Format.printf "%s%s%s%s%s" before color s fin after

let e ?color ?(before="") ?(after="") ?(en=true) s =
  let color, fin = match color with None -> "", "" | Some d -> Format.sprintf "\027[0;%dm" (Color.to_unix_int d), "\027[0m" in
  if en then
    Format.eprintf "%s%s%s%s%s@." before color s fin after
  else
    Format.eprintf "%s%s%s%s%s" before color s fin after
