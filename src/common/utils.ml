(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Proto

let op_hash o = o.op_hash

let reduce_ops (b :full_block) : block = {b with operations = List.map op_hash b.operations}

let operation_bus o =
  match o.man_metadata with
  | None -> []
  | Some meta ->
    let i = List.flatten @@ List.map (fun i -> i.in_result.op_balance_updates)
        meta.man_internal_operation_results in
    meta.man_operation_result.op_balance_updates @ i

let block_operation_bus o =
  List.flatten @@ List.map operation_bus o.op_contents

module SSet = Set.Make(String)

module AccountRelevant = struct

  let mem accounts hash = SSet.mem hash accounts

  let rec list f = function
    | [] -> false
    | h :: t -> f h || list f t

  let kind accounts = function
    | Transaction {destination; _ } -> mem accounts destination
    | Delegation (Some dlg) -> mem accounts dlg
    | _ -> false

  let info accounts m = mem accounts m.source || kind accounts m.kind

  let info_opt accounts o =
    match accounts with None -> true | Some a -> info a o

  let balance_update accounts bu = match bu.bu_update with
    | Contract contract -> mem accounts contract
    | Freezer (Legacy_rewards {fr_delegate; _})
    | Freezer (Legacy_fees {fr_delegate; _})
    | Freezer (Legacy_deposits {fr_delegate; _})-> mem accounts fr_delegate
    | Freezer (Deposits delegate) -> mem accounts delegate
    | Burned (Lost_endorsing_rewards {delegate; _}) -> mem accounts delegate
    | _ -> false

  let meta accounts m =
    list (mem accounts) m.op_originated_contracts ||
    list (balance_update accounts) m.op_balance_updates

  let internal accounts i =
    info accounts i.in_content || meta accounts i.in_result

  let internal_opt accounts o =
    match accounts with None -> true | Some a -> internal a o

  let manager accounts m =
    info accounts m.man_info ||
    (match m.man_metadata with
     | None -> false
     | Some m ->
       list (balance_update accounts) m.man_balance_updates ||
       meta accounts m.man_operation_result ||
       list (internal accounts) m.man_internal_operation_results)

  let manager_opt accounts o =
    match accounts with None -> true | Some a -> manager a o

  let operation accounts o =
    list (manager accounts) o.op_contents

  let operation_opt accounts o =
    match accounts with None -> true | Some a -> operation a o

end

module Process = struct
  open Crp

  let operation f (bindex, oindex0, acc) bl o =
    let|>? bindex, _, acc = fold (fun (bindex, oindex1, acc) m ->
        let bo_meta = Option.map (fun x -> x.man_operation_result) m.man_metadata in
        let>? acc = f acc {
            bo_block = bl.hash; bo_level = bl.header.shell.level;
            bo_tsp = bl.header.shell.timestamp; bo_hash = o.op_hash;
            bo_op = m.man_info; bo_meta; bo_numbers = None;
            bo_nonce = None; bo_counter = m.man_numbers.counter;
            bo_index = bindex; bo_indexes = (oindex0, oindex1, 0l) } in
        let internals = match m.man_metadata with
          | None -> []
          | Some meta -> meta.man_internal_operation_results in
        let|>? (bindex, _, acc) = fold (fun (bindex, oindex2, acc) int_op ->
            let|>? acc = f acc {
                bo_block = bl.hash; bo_level = bl.header.shell.level;
                bo_tsp = bl.header.shell.timestamp; bo_hash = o.op_hash;
                bo_op = int_op.in_content; bo_meta = Some int_op.in_result; bo_numbers = None;
                bo_nonce = Some int_op.in_nonce; bo_counter = m.man_numbers.counter;
                bo_index = bindex; bo_indexes = (oindex0, oindex1, oindex2) } in
            (Int32.succ bindex, Int32.succ oindex2, acc)) (bindex, 1l, acc) internals in
        (bindex, Int32.succ oindex1, acc)) (bindex, 0l, acc) o.op_contents in
    bindex, acc

  let block ?block ?operation:op (acc_b, acc_o) bl =
    let>? acc_o = match op with
      | None -> rok acc_o
      | Some f ->
        let>? _, _, acc_o = fold (fun (bindex, oindex, acc) o ->
            let|>? bindex, acc = operation f (bindex, oindex, acc) bl o in
            bindex, Int32.succ oindex, acc)
            (0l, 0l, acc_o) bl.operations in
        rok acc_o in
    let>? acc_b = match block with
      | None -> rok acc_b
      | Some f -> f acc_b bl in
    rok (acc_b, acc_o)
end
