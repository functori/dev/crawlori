open Crawlori

module Plugin = struct
  include Pg_bigmap.Make(Excraw_common.Contract)
  type extra = Excraw_common.E.extra
  type init_acc = Pg.init_acc
end

let crawler config =
  let open Make(Pg)(Excraw_common.E) in
  Plugins.register_mod (module Plugin);
  Lwt.map (Result.iter_error Rp.print_error) @@
  let>? () = init config in
  loop config

let () =
  match Excraw_common.get_config () with
  | Error e -> Rp.print_error e
  | Ok config -> EzLwtSys.run @@ fun () -> crawler config
